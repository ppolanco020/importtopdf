# ¿Cómo iniciar?

## Guia de la  Ventana

#### 1. Buscar "Solicitud" o "Aval" por número de  **SNIP**
#### 2. Nombre de **Usuario** que ha iniciado sesión.
#### 3. Barra de navegación.
#### 4. Información de  **SIPROCODE**

![login](/images/avales/inicio/2_inicio.png)


## Iniciar Sesión

**PASO 1:**
### Iniciar Sesión

En la pantalla principal debemos presionar el botón **Iniciar Sesión**.

![login](/images/avales/inicio/1_inicio.png)

**PASO 2**
### Ingresar Usuario y Contraseña.

Ingresar el **usuario** y **contraseña** en los campos de *Correo del Usuario* e *Ingrese Contraseña* y pulsar  el botón **Iniciar**.

::: warning Aviso:

Si necesita ayuda para el uso de SIPROCODE, póngase en contacto con su CODEDE.

:::

![login](/images/avales/inicio/login/1_login.png)

**PASO 3**
### Ventana Principal

Al iniciar sesión, aparecerá la ventana principal de **SIPROCODE**

![login](/images/avales/inicio/2_inicio.png)


### Cambiar Contraseña

###  Click en Usuario

Ubicado en la parte superior derecha de **SIPROCODE** encontramos nuestro usuario y nos desplegará la opción **Cambiar contraseña**

![login](/images/avales/inicio/cambiar_contraseña/1_inicio_cambiar_constraseña.png)

**PASO 2**
### Cambiar Contraseña

Seleccionamos la opción **Cambiar Contraseña**.

![login](/images/avales/inicio/cambiar_contraseña/2_inicio_cambiar_constraseña.png)

**PASO 3**
###  Ventana Cambiar contraseña

Colocar la nueva contraseña y confirmarla. Por último presionar el botón **CAMBIAR**


::: warning  Aviso
*La Contraseña debe tener un mínimo de 6 caracteres (debe incluir al menos 1 letra mayúscula y 2 números)*

+ Los campos **Nombre** y **Correo** estarán deshabilitados para su edición.
:::

![login](/images/avales/inicio/cambiar_contraseña/3_inicio_cambiar_constraseña.png)


### Se me olvidó mi Contraseña

::: danger Importante:

Enviar correo a contraseña@scep.gob.gt <br>
Asunto: Recuperar Contraseña  <br>
Mensaje:  Usuario y correo electrónico registrado

:::

## Cerrar Sesión


Ubicado en la parte derecha de **SIPROCODE** hacer click en el nombre de usuario, se desplegará la opción **Cerrar Sesión** y damos click allí.

![login](/images/avales/inicio/cerrar_sesion/1_cerrar_sesion.png)

El sistema nos indicará que la **Sesión ha finalizado**

![login](/images/avales/inicio/cerrar_sesion/2_cerrar_sesion.png)
