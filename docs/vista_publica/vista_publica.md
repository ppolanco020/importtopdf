# Cómo ingresar

En la ventana principal buscamos el botón anaranjado llamado **Vista Pública**


![vista publica](/images/avales/vista_publica/1_vista_publica.png)

En esta ventana se mostrará todos **El total de Solicitudes, Solicitudes nuevas, en progreso, resueltas**. Se podrá filtrar por  **SNIP, entes Rectores o Municipalidades**.

![vista publica](/images/avales/vista_publica/2_vista_publica.png)
